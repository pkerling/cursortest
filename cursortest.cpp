/*
 * Copyright (c) 2014-2019, Nils Christopher Brause, Philipp Kerling, Zsolt Bölöny
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer. 
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
 
// Mostly copied from waylandpp examples/shm.cpp

#include <wayland-client.hpp>
#include "wayland-extra-protocols.hpp"

#include <stdexcept>
#include <iostream>
#include <array>
#include <memory>
#include <sstream>
#include <ctime>
#include <algorithm>

#include <linux/input.h>

#include <sys/mman.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>

using namespace wayland;

// helper to create a std::function out of a member function and an object
template <typename R, typename T, typename... Args>
std::function<R(Args...)> bind_mem_fn(R(T::* func)(Args...), T *t)
{
  return [func, t] (Args... args)
    {
      return (t->*func)(args...);
    };
}

// shared memory helper class
class shared_mem_t
{
private:
  std::string name;
  int fd = 0;
  size_t len = 0;
  void *mem = nullptr;

public:
  shared_mem_t()
  {
  }

  shared_mem_t(size_t size)
  : len(size)
  {
    // Very simple shared memory wrapper - do not use this in production code!
    // The generated memory regions are visible in the file system and can be
    // stolen by other running processes.
    // Linux code should use memfd_create when possible (ommited here for
    // simplicity).
    
    // create random filename
    std::stringstream ss;
    std::srand(std::time(0));
    ss << '/' << std::rand();
    name = ss.str();

    // open shared memory file
    fd = shm_open(name.c_str(), O_RDWR | O_CREAT | O_EXCL, 0600);
    if(fd < 0)
      throw std::runtime_error("shm_open failed.");

    // set size
    if(ftruncate(fd, size) < 0)
      throw std::runtime_error("ftruncate failed.");

    // map memory
    mem = mmap(NULL, len, PROT_READ | PROT_WRITE, MAP_SHARED, fd, 0);
    if(mem == MAP_FAILED)
      throw std::runtime_error("mmap failed.");
  }

  ~shared_mem_t() noexcept(false)
  {
    if(fd)
      {
        if(munmap(mem, len) < 0)
          throw std::runtime_error("munmap failed.");
        if(close(fd) < 0)
          throw std::runtime_error("close failed.");
        if(shm_unlink(name.c_str()) < 0)
          throw std::runtime_error("shm_unlink failed");
      }
  }

  int get_fd()
  {
    return fd;
  }

  void *get_mem()
  {
    return mem;
  }
};

template<typename T>
T clamp(T value, T min, T max)
{
  return std::max(std::min(value, max), min);
}

// example Wayland client
class example
{
private:
  const int WIDTH = 800;
  const int HEIGHT = 600;

  // global objects
  display_t display;
  registry_t registry;
  compositor_t compositor;
  shell_t shell;
  seat_t seat;
  shm_t shm;
  zwp_relative_pointer_manager_v1_t relative_pointer_manager;
  zwp_relative_pointer_v1_t relative_pointer;
  
  // local objects
  surface_t surface;
  shell_surface_t shell_surface;
  pointer_t pointer;
  keyboard_t keyboard;
  callback_t frame_cb;

  std::shared_ptr<shared_mem_t> shared_mem;
  buffer_t buffer[2];
  int cur_buf;

  bool running;
  bool has_pointer;
  bool has_keyboard;
  
  float ptr_x{}, ptr_y{};

  void draw(uint32_t serial = 0)
  {
    uint32_t* pb = static_cast<uint32_t*>(shared_mem->get_mem()) + cur_buf * WIDTH * HEIGHT;
    // fill black
    std::fill_n(pb, WIDTH * HEIGHT, 0xFF000000);
    const int CURSOR_SIZE = 5;
    // draw cursor
    int ptr_x_int = clamp(static_cast<int> (ptr_x), 0, WIDTH - CURSOR_SIZE);
    int ptr_y_int = clamp(static_cast<int> (ptr_y), 0, HEIGHT - CURSOR_SIZE);
    for (int i = 0; i < CURSOR_SIZE; i++)
      std::fill_n(&pb[(ptr_y_int + i) * WIDTH + ptr_x_int], CURSOR_SIZE, 0xFFFFFFFF);

    surface.attach(buffer[cur_buf], 0, 0);
    surface.damage(0, 0, WIDTH, HEIGHT);
    if(!cur_buf)
      cur_buf = 1;
    else
      cur_buf = 0;

    // schedule next draw
    frame_cb = surface.frame();
    frame_cb.on_done() = bind_mem_fn(&example::draw, this);
    surface.commit();
  }

public:
  explicit example(bool use_rp)
  {
    // retrieve global objects
    registry = display.get_registry();
    registry.on_global() = [&] (uint32_t name, std::string interface, uint32_t version)
      {
        if(interface == compositor_t::interface_name)
          registry.bind(name, compositor, version);
        else if(interface == shell_t::interface_name)
          registry.bind(name, shell, version);
        else if(interface == seat_t::interface_name)
          registry.bind(name, seat, version);
        else if(interface == shm_t::interface_name)
          registry.bind(name, shm, version);
        else if(use_rp && interface == zwp_relative_pointer_manager_v1_t::interface_name)
          registry.bind(name, relative_pointer_manager, version);
      };
    display.roundtrip();

    seat.on_capabilities() = [&] (seat_capability capability)
      {
        has_keyboard = capability & seat_capability::keyboard;
        has_pointer = capability & seat_capability::pointer;
      };

    // create a surface
    surface = compositor.create_surface();

    // create a shell surface
    shell_surface = shell.get_shell_surface(surface);
    shell_surface.on_ping() = [&] (uint32_t serial) { shell_surface.pong(serial); };
    shell_surface.set_title("Window");
    shell_surface.set_toplevel();
    surface.commit();

    display.roundtrip();

    // Get input devices
    if(!has_keyboard)
      throw std::runtime_error("No keyboard found.");
    if(!has_pointer)
      throw std::runtime_error("No pointer found.");

    pointer = seat.get_pointer();
    keyboard = seat.get_keyboard();

    // create shared memory
    shared_mem = std::shared_ptr<shared_mem_t>(new shared_mem_t(2*WIDTH*HEIGHT*4));
    auto pool = shm.create_pool(shared_mem->get_fd(), 2*WIDTH*HEIGHT*4);
    for(unsigned int c = 0; c < 2; c++)
      buffer[c] = pool.create_buffer(c*WIDTH*HEIGHT*4, WIDTH, HEIGHT, WIDTH*4, shm_format::argb8888);
    cur_buf = 0;

    // disable cursor
    pointer.on_enter() = [&] (uint32_t serial, surface_t, float x, float y)
      {
        pointer.set_cursor(serial, {}, 0, 0);
        ptr_x = x;
        ptr_y = y;
      };
      
    // Simple pointer position
    pointer.on_motion() = [&] (uint32_t time, float x, float y)
    {
      if (relative_pointer)
        return;

      ptr_x = x;
      ptr_y = y;
    };
    
    if (relative_pointer_manager) 
    {
      relative_pointer = relative_pointer_manager.get_relative_pointer(pointer);
      relative_pointer.on_relative_motion() = [&] (uint32_t utime_hi, uint32_t utime_lo, float dx, float dy, float dx_unaccel, float dy_unaccel)
      {
        ptr_x = clamp(ptr_x + dx, 0.0f, static_cast<float> (WIDTH));
        ptr_y = clamp(ptr_y + dy, 0.0f, static_cast<float> (HEIGHT));
      };
    }
    
    // window movement
    pointer.on_button() = [&] (uint32_t serial, uint32_t time, uint32_t button, pointer_button_state state)
      {
        if(button == BTN_LEFT && state == pointer_button_state::pressed)
          {
            shell_surface.move(seat, serial);
          }
      };

    // press 'q' to exit
    keyboard.on_key() = [&] (uint32_t, uint32_t, uint32_t key, keyboard_key_state state)
      {
        if(key == KEY_Q && state == keyboard_key_state::pressed)
          running = false;
      };

    // draw stuff
    draw();
  }

  ~example()
  {
  }

  void run()
  {
    // event loop
    running = true;
    while(running)
      display.dispatch();
  }
};

int main(int argc, char** argv)
{
  bool use_rp = !(argc == 2 && std::string(argv[1]) == "--no-rp");
  std::cout << "Using relative pointer: " << use_rp << std::endl;
  example e{use_rp};
  e.run();
  return 0;
}
